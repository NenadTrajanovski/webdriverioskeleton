const constants = require("../constants");

class ElementUtil{

    doClick(element){
        element.waitForDisplayed()
        element.click()
    }

    doSetValue(element, value){
        element.waitForDisplayed()
        element.setValue(value)
    }

    doGetText(element){
        element.waitForDisplayed()
        console.log("Element text is: " + element.getText())
        return element.getText()
    }

    doIsDisplayed(element){
        element.waitForDisplayed()
        return element.isDisplayed()
    }

    doGetPageTitle(pageTitle){
        browser.waitUntil(function (){
            return (browser.getTitle() === pageTitle)
        }, 10000, 'Title is not displayed after the given time')
            return browser.getTitle()
    }

    doGetPageMessage(pageMessage, validationString){
        browser.waitUntil(function (){
            return (pageMessage.getText() === validationString)
        }, 4000, 'Message is not displayed')
        assert.equal(pageMessage.getText(), validationString)
    }

    doCloseTab(){
        const handles = browser.getWindowHandles();
        if (handles.length > 1) {
            browser.switchToWindow(handles[1]);
            browser.closeWindow();
        }
    }

    tearDown(){
        browser.reloadSession();
    }






}

module.exports = new ElementUtil();