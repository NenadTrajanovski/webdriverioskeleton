module.exports = {

    //Credentials
    email: "nenad.trajanovski@alan.co",
    password: "123test123",

    //Links
    loginPage: "https://reader.trendmd.com/sign_in",
    signUpPage: "https://reader.trendmd.com/sign_up",
    homePage: "https://reader.trendmd.com/reading_list",
    forgotPassword: "https://reader.trendmd.com/forgot_password",

    termsConditions: "https://trendmd.com/terms",
    privacyPolicy: "https://trendmd.com/privacy",

    privacy: "https://www.trendmd.com/privacy",
    terms: "https://www.trendmd.com/terms",
    helpPage: "https://www.trendmd.com/support",
    optOut: "https://www.trendmd.com/targeting-settings",
    personalInfo: "https://www.trendmd.com/personaldata",
    setting: "https://reader.trendmd.com/settings"


}