const elementUtil = require('../util/elementUtil');
const constants = require('../constants');


class HomePage{

    //ELEMENTS
    get loggedUser(){return $("//div[contains(text(), 'nenad')]")}
    get userMenu(){return $("//div[contains(text(), '@')]")}
    get settingLink() {return $("//li[text() = 'Settings']")}
    get signOutLink() {return $("//li[text() = 'Sign out']")}
    get deleteBtns() {return $("#ic_delete")}

    //READER LIST
    get articleTitle(){return $("//div[text() = 'Article title']")}
    get website(){return $("//div[text() = 'Website']")}
    get readDate(){return $("//div[text() = 'Read date']")}
    get erase(){return $("//div[text() = 'Erase']")}

    //FOOTER LINKS
    get settingsFooter(){return $("//div[text() = 'Settings']")}
    get optOutFooter(){return $("//div[text() = 'TrendMD opt-out']")}
    get personalInfoFooter() {return $("//div[text() = 'Do not sell my personal information']")}

    //METHODS

    cleanReadingList(){
        let deleteButtons = $$("#ic_delete")
        deleteButtons.forEach(element => {
            element.click()
        })
    }

    checkReadingList(){

        if (!this.deleteBtns.isDisplayed()){
            browser.newWindow(constants.LINK_ONE)
            browser.switchWindow(constants.LINK_ONE)
            browser.pause(1000)
        }else {
            this.cleanReadingList()
        }
    }

}

module.exports = new HomePage();