const elementUtil = require('../util/elementUtil');
const constants = require('../constants');


class LoginPage {

    //ELEMENTS Example
    get username(){return $("//input[@type='email']")}
    get password(){return $("//input[@type='password']")}
    get signInButton(){return $("//button")}
    get signUpButton() {return $("//div[text() = 'Sign up']")}
    get forgotPassword() {return $("//a[text() = 'Forgot your password?']")}
    get wrongCredentialMessage() {return $("//span[contains(text(), 'Email')]")}
    get title(){return $("//div[@class = 'auth__name']")}


    //METHODS example

    getPageTitle(){
        return elementUtil.doGetPageTitle();//not finished
    }

    doLogin(emailID, password){
        elementUtil.doSetValue(this.username, emailID);
        elementUtil.doSetValue(this.password, password);
        elementUtil.doClick(this.signInButton);
    }
}

module.exports = new LoginPage();