const elementUtil = require('../util/elementUtil');
const constants = require('../constants');

class SignupPage {

    //ELEMENTS
    get email(){return $("//input[@type= 'email']")}
    get emailValidation(){return $("//p[text() = 'Must be a valid email']")}
    get password(){return $("//input[@type= 'password']")}
    get passwordValidation(){return $("//p[contains(text(), 'Minimum 2 characters')]")}
    get createAccount(){return $("//button[text() = 'Create account']")}


    //CHECKBOXES
    get agreeTermsBox(){return $("//div[contains(text(), 'I agree')]/parent::span//preceding-sibling::div")}
    get newsletterBox(){return $("//span[contains(text() , 'newsletter')]/preceding-sibling::div")}
    get recommendationsBox(){return $("//span[contains(text() , 'recomm')]/preceding-sibling::div")}
    get optInTargeting(){return $("//span[contains(text() , 'target')]/preceding-sibling::div")}
    get consentBox(){return $("//span[contains(text() , 'consent')]/preceding-sibling::div")}
    get checkboxes() {return $("//div[@class = 'checkbox-container']/div/div")}

    //CHECKBOXES TEXT
    get recommendationsBoxText(){return $("//span[contains(text() , 'recomm')]")}
    get optInTargetingText(){return $("//span[contains(text() , 'target')]")}
    get consentBoxText(){return $("//span[contains(text() , 'consent')]")}

    //MESSAGES
    get userExists(){return $("//span[contains(text() , 'User already exists, try again.')]")}

    //LINKS
    get termsConditions() {return $("//a[text() = 'terms and conditions']")}
    get privacyPolicy() {return $("//a[text() = 'privacy policy']")}


    //FOOTER
    get help(){return $("//a[text() = 'Help']")}
    get privacyFooter() {return $("//a[text() = 'Privacy']")}
    get termsFooter() {return $("//a[text() = 'Terms and conditions']")}
    get optOut() {return $("//a[text() = 'TrendMD opt-out']")}
    get personalInfo() {return $("//a[text() = 'Do not sell my personal information']")}

    get signIn(){return $("//div[text() = 'Sign in']")}



    //METHODS example

    getPageTitle(){
        return elementUtil.doGetPageTitle();//not finished
    }

    doSignUpCredentials(emailID, password){
        elementUtil.doSetValue(this.email, emailID);
        elementUtil.doSetValue(this.password, password);
    }

    elementNotClickable(element){
        if (!element.isClickable()) {console.log("Element is disabled")}
    }

    elementNotSelected(checkboxes){
        if (!checkboxes.isSelected()){ return "Element is not selected"}

    }
}

module.exports = new SignupPage();