class ForgetPasswordPage{

    //ELEMENTS
    get emailInput() {return $("//input")}
    get sendResetInstructions(){return $("//button")}
    get validEmailMsg(){return $("//p[text() = 'Must be a valid email']")}
    get noRegUser(){return $("//span[text()='There is no registered user with this email']")}
    get requestSent(){return $("//span[text()='Request sent']")}
}

module.exports = new ForgetPasswordPage()
