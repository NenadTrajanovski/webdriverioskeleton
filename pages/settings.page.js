const constants = require('../constants');
const config = require('../config');
const homePage = require('../pages/home.page');
const elementUtil = require('../util/elementUtil');
const loginPage = require('../pages/login.page')

class SettingsPage{

    //ELEMENTS

    //Additional Settings
    get personalization() {return $("//span[text() = 'Personalization']/preceding-sibling::div")}
    get targeting(){return $("//span[text() = 'Targeting']/preceding-sibling::div")}
    get googleAnalytics(){return $("//span[text() = 'Google Analytics']/preceding-sibling::div")}
    get personalData() {return $("//span[text() = 'Do not sell my personal data']/preceding-sibling::div")}
    get title(){return $("//div[@class = 'title']")}
    get readingList(){return $("//li[text() = 'Reading list']")}

    get passwordChange(){return $("//div[@class = 'password-container']//div[@class = 'button']")}
    get emailChange(){return $("//div[@class = 'email-container']//div[@class = 'button']")}
    get emailInput(){return $("//label[text() = 'Email']/following-sibling::input")}
    get oldPassword(){return $("//input[@name = 'oldPassword']")}
    get newPassword(){return $("//input[@name = 'newPassword']")}
    get confirmPassword(){return $("//input[@name = 'confirmPassword']")}
    get doneBtn(){return $("//button[text() = 'Done']")}
    get incorrectPasswordMessage(){return $("//span[text() = 'Current password is incorrect']")}
    get passwordChangeSuccess(){return $("//span[text() = 'Password updated successfully']")}


    //METHODS
    checkPersonalizationBox(){
        elementUtil.doClick(homePage.userMenu)
        elementUtil.doClick(homePage.settingLink)
        if (this.personalization.isSelected()){
            browser.back()
            browser.pause(1000)
        }else{
            this.personalization.click()
            browser.pause(1000)
            browser.back()
        }
    }

    goToSettingsPage(){
        loginPage.doLogin(config.email, config.password);
        elementUtil.doClick(homePage.userMenu)
        elementUtil.doClick(homePage.settingLink)
    }

    validateMenuOptions(){
        let menuElements = $$("//div[@class = 'header']//ul/li");
        let dropDownList = [];
        menuElements.forEach(element => {
            console.log(element.getText());
            dropDownList.push(element.getText())
        })
        console.log(dropDownList)
        assert.equal(dropDownList[0], "Reading list")
        assert.equal(dropDownList[1], "Sign out")
    }
}
module.exports = new SettingsPage()