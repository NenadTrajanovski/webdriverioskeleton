module.exports = {

    //Validation messages

    WRONG_USER: "Email and password do not match. Please try again",
    EMAIL_VALIDATION: "Must be a valid email",
    PASSWORD_VALIDATION: "Minimum 2 characters. No starting or ending spaces",
    ELEMENT_NOT_SELECTED: "Element is not selected",
    USER_EXISTS: "User already exists, try again.",
    REQUIRED_FIELD: "This field is required",
    NO_REGISTERED_USER: "There is no registered user with this email",
    SEND_FORGOT_PASSWORD: "Request sent",

    //SIGN UP CHECKBOXES TEXT
    TERMS_CONDITIONS: "I agree to the TrendMD",
    TREND_MD_NEWSLETTER: "I want to receive the TrendMD newsletter",
    PERSONALIZED_RECOMMENDATIONS: "I want to see personalized recommendations",
    OPT_IN_TARGETING: "Opt in to TrendMD targeting",
    GOOGLE_ANALYTICS_COOKIES: "I consent to the use of Google Analytics and related cookies",

    //Websites on reader dashboard
    LINK_ONE: "https://www.mdpi.com/2072-6694/13/6/1287",
    LINK_ONE_TITLE: "Oncolytic Virus Therapy Alters the Secretome of Targeted Glioblastoma Cells "



}