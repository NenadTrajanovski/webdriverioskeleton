# WebdriverIO skeleton

This is skeleton for WebDriverIO frame using Mocha.js and Chai.js

**PRE-REQUESTS**
1. Install node.js from https://nodejs.org/en/download/
2. Install git bash from https://git-scm.com/downloads
3. Restart your machine
4. Run these commands from bash or your IDE terminal
    `node -v`
    `npm -v`
it should output the version of both node and npm.



**Project setup**
1. Clone the project to your local drive
1. Open project with IDE of your preference
1. Open IDE terminal, it should point to your project location, if not, navigate to your project location
1. WebDriverIO, Chai.js & Mocha.js are already installed under node_modules
1. To run your test classes from terminal:
- For bulk run use `npm test run` or 
- `npm test -- --spec=test/login.test.js` to run separate classes 
1. This skeleton already contains 3 sample test that you can delete
1. Please do not touch `wdio.conf.js` 
1. Under `package.json` just change the author and nothing else
1. To run allure report `allure generate --clean && allure open`
