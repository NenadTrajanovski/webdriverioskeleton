const loginPage = require('../pages/login.page');
const signUpPage = require('../pages/signup.page');
const homePage = require('../pages/home.page');
const settingsPage = require('../pages/settings.page');
const constants = require('../constants');
const config = require('../config');
const elementUtil = require('../util/elementUtil');

describe('sign up page tests', function () {

    beforeEach(function () {
        browser.url('/');
        browser.maximizeWindow();
        loginPage.doLogin(config.email, config.password);
    })

    afterEach(function () {
        browser.reloadSession();
    })

    it('Check reading list columns', function () {
        assert.equal(elementUtil.doGetText(homePage.articleTitle), "Article title");
        assert.equal(elementUtil.doGetText(homePage.website), "Website");
        assert.equal(elementUtil.doGetText(homePage.readDate), "Read date");
        assert.equal(elementUtil.doGetText(homePage.erase), "Erase");
    });

    it('Dropdown menu validation', function () {
        elementUtil.doClick(homePage.userMenu)
        assert.equal(elementUtil.doGetText(homePage.settingLink), "Settings")
        assert.equal(elementUtil.doGetText(homePage.signOutLink), "Sign out")

        elementUtil.doClick(homePage.settingLink)
        assert.equal(elementUtil.doGetText(settingsPage.title), "Settings")

        elementUtil.doClick(homePage.userMenu)
        elementUtil.doClick(homePage.signOutLink)
        assert.equal(elementUtil.doGetText(loginPage.title), "Sign in")

    });

    it('Remove/Re-add articles', function () {
        /**
         * I've noticed that once an article is removed we cannot load the same article again
         */
    });
})