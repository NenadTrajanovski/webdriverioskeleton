const loginPage = require('../pages/login.page');
const homePage = require('../pages/home.page')
const signUpPage = require('../pages/signup.page')
const configData = require('../config');
const constants = require('../constants');
const elementUtil = require('../util/elementUtil');

describe('login page feature test', function (){

    beforeEach(function (){
        browser.url('/')
        browser.maximizeWindow()
    })

    afterEach(function (){
        browser.reloadSession()
    })

    it('', function () {
        loginPage.doLogin(constants.EMAIL, constants.PASSWORD)
        elementUtil.doGetPageMessage(homePage.loggedUser, constants.EMAIL )
        elementUtil.tearDown()
    });

    it('verify wrong credential message', function (){
        loginPage.doLogin('testqa@testqa.com', 'testqa@testqa.com')
        elementUtil.doGetPageMessage(loginPage.wrongCredentialMessage, constants.WRONG_USER)
    });

    it('get cookies', function (){
        loginPage.doLogin(constants.EMAIL, constants.PASSWORD)
        let trendMD = browser.getCookies()
        console.log("This is test cookie" +trendMD)
    })
})