const loginPage = require('../pages/login.page');
const signUpPage = require('../pages/signup.page');
const homePage = require('../pages/home.page');
const settingsPage = require('../pages/settings.page');
const constants = require('../constants');
const config = require('../config');
const elementUtil = require('../util/elementUtil');

describe('settings page tests', function () {

    beforeEach(function () {
        browser.url('/');
        browser.maximizeWindow();
        settingsPage.goToSettingsPage()
    })

    afterEach(function () {
        browser.reloadSession();
    })


    it('Validate menu list order', function () {
        elementUtil.doClick(homePage.userMenu)
        browser.pause(3000)
        settingsPage.validateMenuOptions()
    });

    it('After sign out user is directed to login page', function () {
        elementUtil.doClick(homePage.userMenu)
        elementUtil.doClick(homePage.signOutLink)
        assert.equal(elementUtil.doGetText(loginPage.title), "Sign in")
    });

    it('Wrong password validation', function () {
        elementUtil.doClick(settingsPage.passwordChange)
        elementUtil.doSetValue(settingsPage.oldPassword, "######")
        elementUtil.doSetValue(settingsPage.newPassword, "123")
        elementUtil.doSetValue(settingsPage.confirmPassword, "123")
        elementUtil.doClick(settingsPage.doneBtn)

        assert.equal(settingsPage.incorrectPasswordMessage.getText(), "Current password is incorrect")
    });

    it('Disable done button on different new and confirm password', function () {
        elementUtil.doClick(settingsPage.passwordChange)
        elementUtil.doSetValue(settingsPage.oldPassword, config.password)
        elementUtil.doSetValue(settingsPage.newPassword, "123")
        elementUtil.doSetValue(settingsPage.confirmPassword, "12345")
        assert.equal(settingsPage.doneBtn.isEnabled(), false)
    });

    it('Enable done button on same new and confirm password', function () {
        elementUtil.doClick(settingsPage.passwordChange)
        elementUtil.doSetValue(settingsPage.oldPassword, config.password)
        elementUtil.doSetValue(settingsPage.newPassword, "12345")
        elementUtil.doSetValue(settingsPage.confirmPassword, "12345")
        assert.equal(settingsPage.doneBtn.isEnabled(), true)

    });

    it('Enable done button on same new and confirm password', function () {
        elementUtil.doClick(settingsPage.passwordChange)
        elementUtil.doSetValue(settingsPage.oldPassword, config.password)
        elementUtil.doSetValue(settingsPage.newPassword, config.password)
        elementUtil.doSetValue(settingsPage.confirmPassword, config.password)

        assert.equal(settingsPage.doneBtn.isEnabled(), true)
        elementUtil.doClick(settingsPage.doneBtn)
        assert.equal(elementUtil.doGetText(settingsPage.passwordChangeSuccess), "Password updated successfully")
    });

    /*it('Change email done button', function () {
        /!**
         * THERE IS BUG with clearValue() and browser.keys this test will fail
         *!/
        elementUtil.doClick(settingsPage.emailChange)
        elementUtil.doSetValue(settingsPage.emailInput, "abc@abc")

        assert.equal(settingsPage.doneBtn.isEnabled(), false)

        elementUtil.doSetValue(settingsPage.emailInput, "abc@abc")
        browser.pause(5000)
        assert.equal(settingsPage.doneBtn.isEnabled(), true)

    });*/
})