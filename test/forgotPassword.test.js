const loginPage = require('../pages/login.page');
const homePage = require('../pages/home.page');
const signUpPage = require('../pages/signup.page');
const forgetPassword = require('../pages/forgetPassword.page');
const configData = require('../config');
const constants = require('../constants');
const elementUtil = require('../util/elementUtil');

describe('forgot password tests', function (){

    beforeEach(function (){
        browser.url('/');
        browser.maximizeWindow();
        elementUtil.doClick(loginPage.forgotPassword);
    })

    afterEach(function (){
        browser.reloadSession();
    })

    it('Correct email validation functionality', function (){
        elementUtil.doSetValue(forgetPassword.emailInput, 'abc@abc');
        browser.keys('Tab')
        assert.equal(elementUtil.doGetText(forgetPassword.validEmailMsg), constants.EMAIL_VALIDATION);
    });

    it('No registered user functionality', function (){
        elementUtil.doSetValue(forgetPassword.emailInput, 'nonexisting@usr.test');
        elementUtil.doClick(forgetPassword.sendResetInstructions)
        assert.equal(elementUtil.doGetText(forgetPassword.noRegUser), constants.NO_REGISTERED_USER);
    })

    it('Request sent functionality', function () {
        elementUtil.doSetValue(forgetPassword.emailInput, configData.email);
        elementUtil.doClick(forgetPassword.sendResetInstructions)
        assert.equal(elementUtil.doGetText(forgetPassword.requestSent), constants.SEND_FORGOT_PASSWORD);
    });
})