const loginPage = require('../pages/login.page');
const signUpPage = require('../pages/signup.page');
const constants = require('../constants');
const config = require('../config');
const elementUtil = require('../util/elementUtil');

describe('sign up page tests', function (){

    beforeEach(function (){
        browser.url('/');
        browser.maximizeWindow();
        elementUtil.doClick(loginPage.signUpButton);
    })

    afterEach(function (){
        browser.reloadSession();
    })

    it('Sign-up with an incorrectly formatted email', function (){
        signUpPage.doSignUpCredentials("abc@abc", '12');
        elementUtil.doClick(signUpPage.agreeTermsBox);
        assert.equal(elementUtil.doGetText(signUpPage.emailValidation), constants.EMAIL_VALIDATION);
    })

    it('Sign-up with an incorrectly formatted password', function (){
        signUpPage.doSignUpCredentials("abc@abc.abc", '1');
        elementUtil.doClick(signUpPage.agreeTermsBox);
        assert.equal(elementUtil.doGetText(signUpPage.passwordValidation), constants.PASSWORD_VALIDATION);
    })

    it('Sign-up without checking checkbox I agree to the TrendMD', function (){
        signUpPage.doSignUpCredentials("abc@abc.abc", '1234');
        signUpPage.elementNotClickable(signUpPage.createAccount);
        elementUtil.doClick(signUpPage.agreeTermsBox);
        signUpPage.createAccount.isClickable();
    })

    it('Check if checkboxes have default values', function (){
        assert.equal(signUpPage.elementNotSelected(signUpPage.agreeTermsBox), constants.ELEMENT_NOT_SELECTED);
        assert.equal(signUpPage.elementNotSelected(signUpPage.newsletterBox), constants.ELEMENT_NOT_SELECTED);
        assert.equal(signUpPage.elementNotSelected(signUpPage.recommendationsBox), constants.ELEMENT_NOT_SELECTED);
        assert.equal(signUpPage.elementNotSelected(signUpPage.optInTargeting), constants.ELEMENT_NOT_SELECTED);
        assert.equal(signUpPage.elementNotSelected(signUpPage.consentBox), constants.ELEMENT_NOT_SELECTED);
    })

    it('Text of checkboxes', function (){
        assert.equal(elementUtil.doGetText(signUpPage.recommendationsBoxText), constants.PERSONALIZED_RECOMMENDATIONS);
        assert.equal(elementUtil.doGetText(signUpPage.optInTargetingText), constants.OPT_IN_TARGETING);
        assert.equal(elementUtil.doGetText(signUpPage.consentBoxText), constants.GOOGLE_ANALYTICS_COOKIES);
    })

    it('Sign-up with a correctly formatted email address and password', function (){
        //need dev environment for this
    })

    it('Create an account with an existing email in DB', function (){
        signUpPage.doSignUpCredentials(config.email, config.email)
        elementUtil.doClick(signUpPage.agreeTermsBox)
        elementUtil.doClick(signUpPage.createAccount)
        assert.equal(elementUtil.doGetText(signUpPage.userExists), constants.USER_EXISTS)
    })

    it('On Sign-up page, links in I agree to the TrendMD [terms and conditions] and [privacy policy] opens correct pages', function () {
        elementUtil.doClick(signUpPage.termsConditions)
        browser.switchWindow(config.termsConditions)
        assert.equal(browser.getUrl(), config.termsConditions)

        elementUtil.doCloseTab()
                browser.switchWindow(config.signUpPage)

        elementUtil.doClick(signUpPage.privacyPolicy)
        browser.switchWindow(config.privacyPolicy)
        assert.equal(browser.getUrl(), config.privacyPolicy)
    });
})