const loginPage = require('../pages/login.page');
const signUpPage = require('../pages/signup.page');
const homePage = require('../pages/home.page')
const constants = require('../constants');
const config = require('../config');
const elementUtil = require('../util/elementUtil');

describe('footer links tests', function (){

    beforeEach(function (){
        browser.url('/');
        browser.maximizeWindow();
    })

    afterEach(function (){
        browser.reloadSession();
    })

    it('Reader Dashboard footer links Sign in page', function () {

        elementUtil.doClick(signUpPage.help)
        browser.switchWindow(config.helpPage)
        assert.equal(browser.getUrl(), config.helpPage)

        browser.switchWindow(config.loginPage)

        elementUtil.doClick(signUpPage.privacyFooter)
        browser.switchWindow(config.privacy)
        assert.equal(browser.getUrl(), config.privacy)

        browser.switchWindow(config.loginPage)

        elementUtil.doClick(signUpPage.termsFooter)
        browser.switchWindow(config.terms)
        assert.equal(browser.getUrl(), config.terms)

        browser.switchWindow(config.loginPage)

        elementUtil.doClick(signUpPage.optOut)
        browser.switchWindow(config.optOut)
        assert.equal(browser.getUrl(), config.optOut)

        browser.switchWindow(config.loginPage)

        elementUtil.doClick(signUpPage.personalInfo)
        browser.switchWindow(config.personalInfo)
        assert.equal(browser.getUrl(), config.personalInfo)
    });

    it('Reader Dashboard footer links Sign up page', function () {
        elementUtil.doClick(loginPage.signUpButton)

        elementUtil.doClick(signUpPage.help)
        browser.switchWindow(config.helpPage)
        assert.equal(browser.getUrl(), config.helpPage)

        browser.switchWindow(config.signUpPage)

        elementUtil.doClick(signUpPage.privacyFooter)
        browser.switchWindow(config.privacy)
        assert.equal(browser.getUrl(), config.privacy)

        browser.switchWindow(config.signUpPage)

        elementUtil.doClick(signUpPage.termsFooter)
        browser.switchWindow(config.terms)
        assert.equal(browser.getUrl(), config.terms)

        browser.switchWindow(config.signUpPage)

        elementUtil.doClick(signUpPage.optOut)
        browser.switchWindow(config.optOut)
        assert.equal(browser.getUrl(), config.optOut)

        browser.switchWindow(config.signUpPage)

        elementUtil.doClick(signUpPage.personalInfo)
        browser.switchWindow(config.personalInfo)
        assert.equal(browser.getUrl(), config.personalInfo)
    });

    it('Reader Dashboard footer links forgot password page', function () {
        elementUtil.doClick(loginPage.forgotPassword)

        elementUtil.doClick(signUpPage.help)
        browser.switchWindow(config.helpPage)
        assert.equal(browser.getUrl(), config.helpPage)

        browser.switchWindow(config.forgotPassword)

        elementUtil.doClick(signUpPage.privacyFooter)
        browser.switchWindow(config.privacy)
        assert.equal(browser.getUrl(), config.privacy)

        browser.switchWindow(config.forgotPassword)

        elementUtil.doClick(signUpPage.termsFooter)
        browser.switchWindow(config.terms)
        assert.equal(browser.getUrl(), config.terms)

        browser.switchWindow(config.forgotPassword)

        elementUtil.doClick(signUpPage.optOut)
        browser.switchWindow(config.optOut)
        assert.equal(browser.getUrl(), config.optOut)

        browser.switchWindow(config.forgotPassword)

        elementUtil.doClick(signUpPage.personalInfo)
        browser.switchWindow(config.personalInfo)
        assert.equal(browser.getUrl(), config.personalInfo)
    });

    it('Reader Dashboard footer links open correct pages when the user is logged in', function () {
        loginPage.doLogin(config.email, config.password)

        elementUtil.doClick(signUpPage.help)
        browser.switchWindow(config.helpPage)
        assert.equal(browser.getUrl(), config.helpPage)

        browser.switchWindow(config.homePage)

        elementUtil.doClick(signUpPage.privacyFooter)
        browser.switchWindow(config.privacy)
        assert.equal(browser.getUrl(), config.privacy)

        browser.switchWindow(config.homePage)

        elementUtil.doClick(signUpPage.termsFooter)
        browser.switchWindow(config.terms)
        assert.equal(browser.getUrl(), config.terms)

        browser.switchWindow(config.homePage)

        elementUtil.doClick(homePage.settingsFooter)
        browser.switchWindow(config.setting)
        assert.equal(browser.getUrl(), config.setting)

        elementUtil.doClick(homePage.optOutFooter)
        assert.equal(browser.getUrl(), config.setting)

        elementUtil.doClick(homePage.personalInfoFooter)
        assert.equal(browser.getUrl(), config.setting)
    });
})